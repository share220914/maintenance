function changeLanguage(lang) {
    localStorage.setItem("language", lang)
    location.reload();
}
var language = {
    vi: {
        titlePage: "Hệ thống đang bảo trì - YSwinner",
        titleHeader: "HỆ THỐNG UAT ĐANG BẢO TRÌ",
        textCountdown: "Thời gian: Từ 18:00, 11/08/2023 đến 11:00, 12/08/2023",
        textContent_1: "Hệ thống đang chuyển đổi UAT - KRX",
        textContent_2: "Quý khách vui lòng quay lại sau. Xin lỗi vì sự bất tiện này.",
        textCountdownTime: ["Ngày", "Giờ", "Phút", "Giây"]
    },
    en: {
        titlePage: "System under maintenance - YSwinner",
        titleHeader: "THE SYSTEM UAT IS MAINTENANCE",
        textCountdown: "From 18:00, 2023/08/11 to 11:00 2023/08/12",
        textContent_1: "The system is converting UAT - KRX",
        textContent_2: "Please come back later. Sorry for the inconvenience.",
        textCountdownTime: ["Date", "Hour", "Minute", "Second"]
    },
    zh: {
        titlePage: "系統正在維護 - YSwinner",
        titleHeader: "系統正在維護[UAT]",
        textCountdown: "從2023年08月11日 - 6點下午 到 2023年08月12日 - 5點下午",
        textContent_1: "系统正在转换 UAT - KRX",
        textContent_2: "請待會再回來。請諒解為了帶來不便敬。",
        textCountdownTime: ["Date", "Hour", "Minute", "Second"]
    }
};
setInterval(function () {
        if (localStorage.getItem("language") === null)
        {
            localStorage.setItem("language", "vi")
            location.reload();
        }
})

if (window.location) {
    if (localStorage.getItem("language") == "vi") {
        langTitlePage.textContent = language.vi.titlePage;
        langTitleHeader.textContent = language.vi.titleHeader;
        langtextContent_1.textContent = language.vi.textContent_1;
        langtextContent_2.textContent = language.vi.textContent_2
        if (document.querySelector('.countdown') !== null) {
            langtextCountdown.textContent = language.vi.textCountdown;
            langtextCountdownDays.textContent = language.vi.textCountdownTime[0]
            langtextCountdownHours.textContent = language.vi.textCountdownTime[1]
            langtextCountdownMinutes.textContent = language.vi.textCountdownTime[2]
            langtextCountdownSeconds.textContent = language.vi.textCountdownTime[3]
        }
    }
    else if (localStorage.getItem("language") == "en") {
        langTitlePage.textContent = language.en.titlePage;
        langTitleHeader.textContent = language.en.titleHeader;
        langtextContent_1.textContent = language.en.textContent_1;
        langtextContent_2.textContent = language.en.textContent_2;
        if (document.querySelector('.countdown') !== null) {
            langtextCountdown.textContent = language.en.textCountdown;
            langtextCountdownDays.textContent = language.en.textCountdownTime[0]
            langtextCountdownHours.textContent = language.en.textCountdownTime[1]
            langtextCountdownMinutes.textContent = language.en.textCountdownTime[2]
            langtextCountdownSeconds.textContent = language.en.textCountdownTime[3]
        }
    }
    else if (localStorage.getItem("language") == "zh") {
        langTitlePage.textContent = language.zh.titlePage;
        langTitleHeader.textContent = language.zh.titleHeader;
        langtextContent_1.textContent = language.zh.textContent_1;
        langtextContent_2.textContent = language.zh.textContent_2;
        if (document.querySelector('.countdown') !== null) {
            langtextCountdown.textContent = language.zh.textCountdown;
            langtextCountdownDays.textContent = language.zh.textCountdownTime[0]
            langtextCountdownHours.textContent = language.zh.textCountdownTime[1]
            langtextCountdownMinutes.textContent = language.zh.textCountdownTime[2]
            langtextCountdownSeconds.textContent = language.zh.textCountdownTime[3]
        }
    }
}